#!/bin/bash
#check if a filename argument was provided
if [[ $# -eq 0 ]]; then
  echo "Please pfovide a filename as an argument."
  exit 1
fi

filename=$1
echo "The filename passed is $filename"

#search for the files in the entire file system
location=$(sudo find / -name "$filename" 2>/dev/null)

if [[ -n "$location" ]]; then
	  echo "The file '$filename' was found at: $location"
  else   
	    echo "The file '$filename' was not found in the file system"


fi
