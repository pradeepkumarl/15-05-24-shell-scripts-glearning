#!/bin/bash
#
echo "Enter a fruit name: "

read fruit

case "$fruit" in
  "apple") echo "The fruit is an apple."
           ;;	  
  "banana") echo "The fruit is a banana."
           ;;	  
  "orange") echo "The fruit is an orange."
           ;;	  
  *) echo "Unknown."
           ;;	  
esac	   
	   
