#!/bin/bash
#This script will print numbers from 1 to 10 using while loop
counter=1

while [ $counter -le 10 ]
do
  echo "Number: $counter"
    ((counter++))
done

