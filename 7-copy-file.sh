
#check if the correct number of arguments are provided
if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <source_file> <destination_directory"
	exit 1
fi

#Assign the arguments to the variables
source_file=$1
destination_directory=$2

#check if the source file is present
if [ ! -f "$source_file" ]; then
  echo "Source file does not exists: $source_file"
  exit 1
fi
#check if the destination directory is present
if [ ! -d "$destination_directory" ]; then
  echo "Destination directory does not exists: $destination_diretory"
  exit 1
fi

#copy the files to the destination directory
cp "$source_file" "$destination_directory"

if [ $? -eq 0 ]; then
  echo "File $soure_file" successfully copied to "$destination_directory"
else  
  echo "Error: Failed to copy File $soure_file" successfully copied to "$destination_directory"
  exit 1
fi  


