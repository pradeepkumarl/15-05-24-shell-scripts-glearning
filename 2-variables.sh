#!/bin/bash
echo "demonstrating the variables in bash script"
name="Vinay"
age=25
city="Bangalore"
result=$(ls -l)
totalValue=$((10 + 25))
currentDate=$(date)
currentOS=$(uname -a)

echo "total Value: $totalValue"
echo "current Date: $currentDate"
echo "current OS: $currentOS"
