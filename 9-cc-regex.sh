#!/bin/bash
#
credit_card_pattern='^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}$'

read -p "Enter credit card number: " credit_card_number

if [[ $credit_card_number =~ $credit_card_pattern ]]; then
  echo "Valid credit card number."
  else
    echo "Invalid credit card number."
fi

