#check if the correct number of arguments are provided
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <source_directory> <destination_directory"
  exit 1
fi

#Assign the arguments to the variables
source_dir=$1
destination_directory=$2
tar_file="output.tar"

#check if the source directory is present
if [ ! -d "$source_dir" ]; then
  echo "Source directory does not exists: $source_dir"
  exit 1
fi
#check if the destination directory is present
if [ ! -d "$destination_directory" ]; then
  echo "Destination directory does not exists: $destination_diretory"
  exit 1
fi

# naviage to the source directory
cd $source_dir
#remove any tar file if exists
rm *.tar
#create the tar file
tar -cvf $tar_file *

#copy the tar file to destination directory
mv *.tar $destination_directory
#exit the direcoty
cd -

#
#
#
